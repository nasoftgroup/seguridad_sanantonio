package com.sanantonio.seguridad.Functions;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by nicolas on 5/9/17.
 */

public class funciones {

    public static void sendSms(String number, String msg) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, null, msg, null, null);
    }

    public static Boolean GetMetodo(Context context) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        return SP.getBoolean("Sms", false);
    }

    public static String GetConfigString(Context context, String key) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        return SP.getString(key, "");
    }

    public static boolean isOnline(Activity activity) {
        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static boolean isOnlineThread(Activity activity) {
        try {
            URL myUrl = new URL("http://www.sanantonioseguro.com");
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(2000);
            connection.connect();
            return true;
        } catch (Exception e) {
            return false;
        }


    }

    public static void SetString(Context context, String key, String value) {
        SharedPreferences SP = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = SP.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String GetString(Context context, String key) {
        SharedPreferences SP = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        return SP.getString(key, "");
    }
}
