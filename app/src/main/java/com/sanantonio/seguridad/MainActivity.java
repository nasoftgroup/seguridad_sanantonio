package com.sanantonio.seguridad;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sanantonio.seguridad.Config.ConfigFragment;
import com.sanantonio.seguridad.Fragments.info_fragment;
import com.sanantonio.seguridad.Functions.funciones;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        setTitle("San antonio seguro");


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        verificarPermisos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), ConfigActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else if (id == R.id.exit) {
            funciones.SetString(MainActivity.this, "telefonoid", "");
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void verificarPermisos() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    return;
                }
            }
        }

    }


    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        TextView lblDetener;
        FloatingActionButton fab;
        ImageButton imgP;
        ImageButton imgH;
        ImageButton imgD;
        ImageButton imgB;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            lblDetener = (TextView) rootView.findViewById(R.id.lblDetener);
            fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
            imgH = (ImageButton) rootView.findViewById(R.id.imgH);
            imgD = (ImageButton) rootView.findViewById(R.id.imgD);
            imgB = (ImageButton) rootView.findViewById(R.id.imgB);
            imgP = (ImageButton) rootView.findViewById(R.id.imgP);

            imgH.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableIb(false);
                    detener("H");
                }
            });
            imgD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableIb(false);
                    detener("D");
                }
            });
            imgB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableIb(false);
                    detener("B");
                }
            });
            imgP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableIb(false);
                    detener("P");
                }
            });
            return rootView;
        }

        private void detener(final String type) {

            String celularDeEnvio = funciones.GetConfigString(getContext(), "Celular");
            boolean sms = funciones.GetMetodo(getContext());
            if (celularDeEnvio.isEmpty() && !funciones.isOnline(getActivity())) {
                Snackbar.make(this.getView(), "Configure el número celular de destino o conéctese a internet antes de enviar la alerta.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                disableIb(true);
                return;
            }
            fab.setVisibility(View.VISIBLE);
            lblDetener.setVisibility(View.VISIBLE);
            final Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                private Handler updateUI = new Handler() {
                    int second = 6;

                    @Override
                    public void dispatchMessage(Message msg) {
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                timer.cancel();
                                cancelAlert();
                                fab.setVisibility(View.INVISIBLE);
                                lblDetener.setVisibility(View.INVISIBLE);
                            }
                        });
                        super.dispatchMessage(msg);
                        second = second - 1;
                        String text = "Detener en [ " + second + " ]";
                        lblDetener.setText(text);
                        disableIb(false);
                        if (second == 0) {
                            SendAlert(type);
                            fab.setVisibility(View.INVISIBLE);
                            lblDetener.setVisibility(View.INVISIBLE);
                            timer.cancel();
                        }

                    }
                };

                public void run() {
                    try {
                        updateUI.sendEmptyMessage(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 0, 1000);
        }

        private void SendAlert(final String type) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.show();
            progressDialog.setMessage("Enviando...");
            progressDialog.setCancelable(false);
            disableIb(true);
            final String data = "Notificación de alerta enviada";
            MediaPlayer sBad = MediaPlayer.create(getContext(), R.raw.boing2);
            final MediaPlayer sOk = MediaPlayer.create(getContext(), R.raw.doorbell_x);
            try {
                final String celularDeEnvio = funciones.GetConfigString(getContext(), "Celular");
                if (!funciones.isOnline(getActivity())) {
                    funciones.sendSms(celularDeEnvio, type + ",M");
                    sOk.start();
                    Snackbar.make(this.getView(), data, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    progressDialog.dismiss();
                } else {
                    String url = funciones.GetString(getContext(), "urlBase") + "logic/alerts.php?add=" + funciones.GetString(getContext(), "telefonoid") + "," + type + ",M";
                    JsonObjectRequest stringRequest = new JsonObjectRequest(JsonObjectRequest.Method.GET, url, new JSONObject(), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject object) {
                            try {
                                String r = object.getString("R");
                                switch (r) {
                                    case "1":
                                        sOk.start();
                                        Snackbar.make(getView(), data, Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show();
                                        break;
                                    case "0":
                                        break;
                                    case "-1":
                                        break;
                                    case "-2":
                                        Snackbar.make(getView(), "Telefono no registrado", Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show();
                                        break;
                                }
                                progressDialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                try {
                                    funciones.sendSms(celularDeEnvio, type + ",M");
                                    sOk.start();
                                    Toast.makeText(getActivity(), data, Toast.LENGTH_LONG).show();
                                } catch (Exception ex) {
                                    Toast.makeText(getActivity(), "Se presentó un error", Toast.LENGTH_LONG).show();
                                }
                                progressDialog.dismiss();
                            }
                        }


                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    try {
                                        funciones.sendSms(celularDeEnvio, type + ",M");
                                        sOk.start();
                                        Toast.makeText(getActivity(), data, Toast.LENGTH_LONG).show();
                                    } catch (Exception ex) {
                                        Toast.makeText(getActivity(), "Se presentó un error", Toast.LENGTH_LONG).show();
                                    }
                                    progressDialog.dismiss();
                                }
                            });
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(1000, 1, 1));
                    RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                    requestQueue.add(stringRequest);
                }

            } catch (Exception e) {
                sBad.start();
                Snackbar.make(this.getView(), "Se presentó un error al envíar la alerta.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                progressDialog.dismiss();
            }

        }

        private void cancelAlert() {
            disableIb(true);
            String data = "Notificación de alerta cancelada";
            try {
                Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                String error = e.getMessage();
            }

        }

        private void disableIb(boolean estado) {
            imgH.setEnabled(estado);
            imgD.setEnabled(estado);
            imgB.setEnabled(estado);
            imgP.setEnabled(estado);
        }


    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment frag = null;
            if (position == 0)
                return PlaceholderFragment.newInstance(position + 1);
            else
                return new info_fragment();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Alertas";
                case 1:
                    return "Información";
            }
            return null;
        }
    }
}
