package com.sanantonio.seguridad.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sanantonio.seguridad.Functions.funciones;
import com.sanantonio.seguridad.LoginActivity;
import com.sanantonio.seguridad.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nicolas on 5/9/17.
 */

public class info_fragment extends Fragment {
    View rootView;
    TextView id_nombre;
    TextView id_link;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_info, container, false);
        id_nombre = (TextView) rootView.findViewById(R.id.id_nombre);
        id_link = (TextView) rootView.findViewById(R.id.id_link);
        id_link.setText("Ver alertas");
        id_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.sanantonioseguro.com/app/map.php?map=1&user=" + funciones.GetString(getActivity(), "user") + "&pass=" + funciones.GetString(getActivity(), "pass"))));
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        configAll();
        super.onResume();
    }

    private void configAll() {

        LinearLayout card = (LinearLayout) rootView.findViewById(R.id.info_card);
        if (!funciones.isOnline(getActivity())) {
            card.setVisibility(View.INVISIBLE);
        } else {
            card.setVisibility(View.VISIBLE);

            String nombres = funciones.GetString(getActivity(), "Nombres");
            String apellido = funciones.GetString(getActivity(), "Apellidos");
            String direccion = funciones.GetString(getActivity(), "Direccion");
            String telefono = funciones.GetString(getActivity(), "telefonoid");

            id_nombre.setText(
                    "Nombre: " + nombres + " " + apellido + "\n" +
                            "Dirección: " + direccion + "\n" +
                            "Teléfono: " + telefono + "\n"
            );

        }
    }
}
