package com.sanantonio.seguridad;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.ExecutorDelivery;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sanantonio.seguridad.Functions.funciones;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    EditText usuario;
    EditText password;
    Button btnlogin;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        usuario = (EditText) findViewById(R.id.usuario);
        password = (EditText) findViewById(R.id.password);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Cargardo...");
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        LogIn();
                    }
                });

            }
        });
    }

    public void LogIn() {
        dialog.show();
        if (!funciones.isOnline(LoginActivity.this)) {
            dialog.dismiss();
            Toast.makeText(LoginActivity.this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        } else {
            GetInfoUser();
        }

    }

    private void GetInfoUser() {
        String user = usuario.getText().toString();
        String pass = password.getText().toString();
        String baseurl = funciones.GetString(LoginActivity.this, "urlBase");
        String url = baseurl + "logic/users.php?apia=1&user=" + user + "&pass=" + pass + "";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (SetInfoData(response)) {
                    dialog.dismiss();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    dialog.dismiss();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Toast.makeText(LoginActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
    }

    private boolean SetInfoData(String response) {
        try {
            JSONObject object = new JSONObject(response);
            String nombres = object.getString("Nombres");
            String apellido = object.getString("Apellidos");
            String direccion = object.getString("Direccion");
            String operador = object.getString("Operador");
            String telefono = object.getString("Telefono");
            String user = usuario.getText().toString();
            String pass = password.getText().toString();
            funciones.SetString(LoginActivity.this, "telefonoid", operador + telefono);
            funciones.SetString(LoginActivity.this, "Nombres", nombres);
            funciones.SetString(LoginActivity.this, "Apellidos", apellido);
            funciones.SetString(LoginActivity.this, "Direccion", direccion);
            funciones.SetString(LoginActivity.this, "user", user);
            funciones.SetString(LoginActivity.this, "pass", pass);
            return true;
        } catch (JSONException e) {
            Toast.makeText(LoginActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_LONG).show();
            return false;
        } catch (Exception ex) {
            Toast.makeText(LoginActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
