package com.sanantonio.seguridad.Config;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.sanantonio.seguridad.R;

/**
 * Created by nicolas on 5/9/17.
 */

public class ConfigFragment extends PreferenceFragment {
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.config_preference);
    }
}